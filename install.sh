#!/usr/bin/env bash

HERE="$(dirname "$0")"

BINS="$HOME/.local/bin"

if [[ "$UID" = 0 ]]; then
    BINS="/usr/local/bin"
fi

mkdir -p "$BINS"

cp "$HERE/src/chunk_hexdump.py" "$HERE/src/bdiff.sh" "$BINS"

echo "Installed."
