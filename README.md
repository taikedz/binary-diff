# Binary Diff

Detecting small, local changes in binary files.

This is a small naive project I started because I remember one day wondering, "how would I visually diff binary files for small subtle changes?"

## Usage

It's a very simple item - pass `bdiff.sh` two binary files to check, and supplement with any other `diff` command options.

```sh
bdiff.sh FILE1 FILE2 [DIFFOPTIONS ...]
```

For example

```sh
bdiff.sh file1.out file2.out -u --color
```

where `-u` and `--color` are simply passed along to the `diff` command.

## Reason

There are some lackluster answers on StackOverflow that suggest to "just use hexdump and then diff." ([like this][stacko])

[stacko]: https://superuser.com/questions/125376/how-do-i-compare-binary-files-in-linux

The problem is that if you use `hexdump`, you can be guaranteed that after the first change, unless the byte count remained the same between the files, the *entire* remainder of the stream (swathes of null-bytes notwithstanding) will be different from its counterpart for each file, owing to the offset codes and the displacement of the bytes.

This tool gets around that by artificially re-introducing the concept of "chunks". Where `diff` is useful is in showing which lines of source code have changed, and then recorgnising the rest as "unchanged", but to do so it has to work on its smallest data sequence - the line. There are no  "lines" in binary files, so instead we pre-process the binary data split it into chunks. In this way, even if the byte count at the point of change is different, `diff` will only show which *chunk* was affected. Once the chunk ends, a new chunk begins in each file, and since these are now identical, `diff` doesn't continue marking the two as different.

The pre-processor `chunk_hexdump.py` determines the end of a chunk as being the end of a sequence of `null` bytes, line-feed bytes, or carriage-return bytes (an assortment of such bytes in one sequence is a single delimiter).

A longer write-up is on [dev.to/taikedz](https://dev.to/taikedz/a-slightly-less-naive-binary-diff-294i)
