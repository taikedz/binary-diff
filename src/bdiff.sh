#!/usr/bin/env bash

file1="$1"; shift
file2="$1"; shift

check_file() {
    [[ -f "$1" ]] || {
        echo "'$1' is not a file"
        exit 1
    }
}

check_file "$file1"
check_file "$file2"

exec diff "$@" \
    <(python3 "$(dirname "$0")"/chunk_hexdump.py "$file1") \
    <(python3 "$(dirname "$0")"/chunk_hexdump.py "$file2")
